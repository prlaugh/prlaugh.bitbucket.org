(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

/**
 * account
 * @requires
 */

var _m_ = {

    init: function init() {

        //Show first tab
        $('#account-table a:first').tab('show');
        //Init datatable
        var table = $('.table').DataTable({
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            responsive: true,
            "order": [[0, "desc"]]
        });

        //Rebuild table on tab swap
        $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
            console.log('show tab');
            table.responsive.recalc();
            table.columns.adjust();
        });

        //Temp active toggle
        $('.Quizzes-start ol .btn').click(function () {
            // $('.Quizzes-start ol .btn').not(this).removeClass('btn-secondary').addClass('btn-white');
            $(this).toggleClass('btn-white').toggleClass('btn-secondary');
        });
    }
};
module.exports = _m_;

},{}],2:[function(require,module,exports){
"use strict";

/**
 * awards
 * @requires
 */

var _m_ = {

    init: function init() {

        //Init datatable
        var table = $('.table').DataTable({
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            responsive: true,
            "order": [[1, "desc"]]
        });
    }
};
module.exports = _m_;

},{}],3:[function(require,module,exports){
'use strict';

var _m_ = {

    init: function init() {

        // Init tooltips
        // $('[data-toggle="tooltip"]').tooltip();

        // Mobile Navigation
        var mobileMenu = $('.Topbar-mobile-menu'),
            siteOverlay = $('.site-overlay'),
            mobileSearch = $('.Topbar-search');

        $(mobileMenu).click(function () {
            $(this).toggleClass('is-open');
        });

        $(siteOverlay).click(function () {
            $(mobileMenu).removeClass('is-open');
        });

        // Mobile Search
        var start;
        $(document).bind('touchstart', function (e) {
            start = e.originalEvent.touches[0].clientY;
        });

        $(document).bind('touchmove', function (e) {
            var end = e.originalEvent.changedTouches[0].clientY;
            if (start > end) {
                $(mobileSearch).removeClass('is-sticky');
                $(mobileSearch).addClass('is-static');
            } else {
                $(mobileSearch).removeClass('is-static');
                $(mobileSearch).addClass('is-sticky');
            }
        });

        if ($('div').attr('data-mh') === 'true') {
            $(this).matchHeight();
        }

        // Modal
        function showAjaxSpinner(message) {
            if (message != null && message != '') {
                $('p#ajax-msg').html = message;
            }
            $('div#ajaxModal').modal({
                keyboard: true,
                backdrop: true,
                show: true
            });
        }

        function hideAjaxSpinner() {
            $('div#ajaxModal').modal('hide');
        }

        function showStandardErrorModal(msg) {
            $('div#standardErrorModal p#error-message').text(msg);
            $('div#standardErrorModal').modal({
                keyboard: true,
                backdrop: false,
                show: true
            });
        }

        // app specific 

        if (window.location.href.indexOf("orange") > -1) {
            $('table').removeClass('table-striped');
        }
    }
};
module.exports = _m_;

},{}],4:[function(require,module,exports){
/**
 * catalog
 */
//
// var _m_ = {
//
//     init: function() {
//     }
// };
// module.exports = _m_;
"use strict";

},{}],5:[function(require,module,exports){
'use strict';

var _sliderData = require('../data/sliderData');

var _m_ = {

    init: function init() {

        function commaSeparateNumber(val) {
            while (/(\d+)(\d{3})/.test(val.toString())) {
                val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
            }
            return val;
        }

        var pointVal = parseInt($('.Dashboard-points-total').data('total'));
        $({
            pointCount: 0
        }).animate({
            pointCount: pointVal
        }, {
            duration: 1500,
            easing: 'swing',
            step: function step() {
                $('.Dashboard-points-total').text(commaSeparateNumber(Math.ceil(this.pointCount)));
            }
        });

        var volPercent = parseInt($('.progress-bar').data('volume'));
        $('.progress-bar').animate({
            width: volPercent + '%'
        }, 1500);
    }
}; /**
    * dashboard
    * @requires sliderData
    */

module.exports = _m_;

},{"../data/sliderData":11}],6:[function(require,module,exports){
'use strict';

var _sliderData = require('../data/sliderData');

var _m_ = {

    init: function init() {}
}; /**
    * home
    * @requires sliderData
    */

module.exports = _m_;

},{"../data/sliderData":11}],7:[function(require,module,exports){
'use strict';

var _sliderData = require('../data/sliderData');

var _m_ = {

    init: function init() {}
}; /**
    * leaderboard
    * @requires sliderData
    */

module.exports = _m_;

},{"../data/sliderData":11}],8:[function(require,module,exports){
'use strict';

/**
 * quizzes
 * @requires
 */

var _m_ = {

    init: function init() {

        //Show first tab
        $('#product-table a:first').tab('show');
        //Init datatable
        var table = $('.table').DataTable({
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            responsive: true,
            "order": [[1, "desc"]]
        });

        //Rebuild table on tab swap
        $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
            // table.responsive.recalc();
            // table.columns.adjust();

        });
    }
};
module.exports = _m_;

},{}],9:[function(require,module,exports){
'use strict';

/**
 * quizzes
 * @requires
 */

var _m_ = {

    init: function init() {

        //Show first tab
        $('#product-table a:first').tab('show');
        //Init datatable
        var table = $('.table').DataTable({
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            responsive: true,
            "order": [[1, "desc"]]
        });

        //Rebuild table on tab swap
        $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
            console.log('show tab');
            table.responsive.recalc();
            table.columns.adjust();
        });

        //Temp active toggle
        $('.Quizzes-start ol .btn').click(function () {
            // $('.Quizzes-start ol .btn').not(this).removeClass('btn-secondary').addClass('btn-white');
            $(this).toggleClass('btn-white').toggleClass('btn-secondary');
        });
    }
};
module.exports = _m_;

},{}],10:[function(require,module,exports){
'use strict';

/**
 * quizzes
 * @requires
 */

var _m_ = {

    init: function init() {

        $('#dp1').datepicker();
    }
};
module.exports = _m_;

},{}],11:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var breakpoints = [{
    breakpoint: 0,
    settings: {
        slidesToShow: 2,
        slidesToScroll: 2
    }
}, {
    breakpoint: 480,
    settings: {
        slidesToShow: 2,
        slidesToScroll: 2
    }
}, {
    breakpoint: 768,
    settings: {
        slidesToShow: 3,
        slidesToScroll: 3
    }
}, {
    breakpoint: 992,
    settings: {
        slidesToShow: 4,
        slidesToScroll: 4
    }
}, {
    breakpoint: 1200,
    settings: {
        slidesToShow: 6,
        slidesToScroll: 6
    }
}],
    transform = 'cubic-bezier(0.4,0,0.2,1)';

var heroSlider = $('.Hero-slider').imagesLoaded(function () {
    $('.Hero-slider').slick({
        dots: true,
        arrows: false,
        speed: 500,
        useTransform: true,
        cssEase: transform,
        appendDots: '.Hero',
        lazyLoad: 'ondemand',
        autoplay: true,
        autoplaySpeed: 5000
        // infinite: false
    });
});

var getNowSlider = $('.Get-now.slider, .Get-now-slider').slick({
    dots: false,
    arrows: true,
    appendArrows: '.Get-now .col-md-12',
    infinite: false,
    speed: 200,
    useTransform: true,
    cssEase: transform,
    mobileFirst: true,
    responsive: breakpoints
});

var trendingSlider = $('.Trending-rewards.slider, .Trending-rewards-slider').slick({
    dots: false,
    arrows: true,
    appendArrows: '.Trending-rewards .col-md-12',
    infinite: false,
    speed: 200,
    useTransform: true,
    cssEase: transform,
    mobileFirst: true,
    responsive: breakpoints
});

exports.breakpoints = breakpoints;
exports.transform = transform;

},{}],12:[function(require,module,exports){
(function (global){
'use strict';

/**
 * main
 * @Main js file
 */
// var Router = require('./lib/router');

// polyfill flexbox
flexibility(document.documentElement);

var app = {
  base: require('./controllers/base'),
  home: require('./controllers/home'),
  dashboard: require('./controllers/dashboard'),
  quizzes: require('./controllers/quizzes'),
  account: require('./controllers/account'),
  leaderboard: require('./controllers/leaderboard'),
  reports: require('./controllers/reports'),
  salesEntry: require('./controllers/salesEntry'),
  catalog: require('./controllers/catalog'),
  awards: require('./controllers/awards'),
  bootstrap: function bootstrap() {
    $(document).ready(app.docready);
  },
  bindEvents: function bindEvents() {
    $('.tags .glyphicon-remove').on('click', function () {
      $(this).closest('li').remove();
    });
    $('.multi-filter__clear-all').on('click', function (e) {
      e.preventDefault();
      $('.tags').empty();
    });
    $('.multi-filter__filter-action').on('click', function (e) {
      console.log(e);
      var $el = $(this);
      var glyph = $('.glyphicon', $el);
      if ($('a[role="button"]', $el).hasClass('collapsed')) {
        glyph.addClass('glyphicon-minus');
        glyph.removeClass('glyphicon-plus');
      } else {
        glyph.removeClass('glyphicon-minus');
        glyph.addClass('glyphicon-plus');
      }
    });
  },
  docready: function docready() {
    app.bindEvents();

    $('main').each(function () {
      // load base js for all pages
      app.base.init();

      // load page-specific js
      if ($(this).attr('id') === 'Home') {
        app.home.init();
      }
      if ($(this).attr('id') === 'Dashboard') {
        console.log('dashboard');
        app.dashboard.init();
      }
      if ($(this).attr('id') === 'Quizzes') {
        app.quizzes.init();
      }
      if ($(this).attr('id') === 'AccountBalance') {
        app.account.init();
      }
      if ($(this).attr('id') === 'LeaderBoard') {
        app.leaderboard.init();
      }
      if ($(this).attr('id') === 'Reporting') {
        app.reports.init();
      }
      if ($(this).attr('id') === 'SalesEntry') {
        app.salesEntry.init();
      }
      if ($(this).attr('id') === 'Catalog') {
        app.catalog.init();
      }
      if ($(this).attr('id') === 'Awards') {
        app.awards.init();
      }
      if ($(this).attr('id') === 'PastNominations') {
        app.awards.init();
      }
    });

    // app.router = new Router();
    // // Add routes
    // app.router.addRoutes([
    //     {   // home page
    //         path: '/([^/]*)',
    //         xcel: [app.base.init, app.home.init],
    //         destroy: [app.base.init, app.home.destroy]
    //     },
    //     {   // dashboard
    //         path: '/dashboard/([^/]*)',
    //         xcel: [app.base.init, app.dashboard.init],
    //         destroy: [app.base.init, app.dashboard.destroy]
    //     },
    //     {   // quizzes
    //         path: '/quizzes/([^/]*)',
    //         xcel: [app.base.init, app.quizzes.init],
    //         destroy: [app.base.init, app.quizzes.destroy]
    //     },
    //     {   // account
    //         path: '/account/([^/]*)',
    //         xcel: [app.base.init, app.account.init],
    //         destroy: [app.base.init, app.account.destroy]
    //     },
    //     {   // leaderboard
    //         path: '/leaderboard/([^/]*)',
    //         xcel: [app.base.init, app.leaderboard.init],
    //         destroy: [app.base.init, app.leaderboard.destroy]
    //     },
    //     {   // reports
    //         path: '/reports/([^/]*)',
    //         xcel: [app.base.init, app.reports.init],
    //         destroy: [app.base.init, app.reports.destroy]
    //     },
    // ]);

    // app.router.doRoute();
  }
};
global.app = app;
app.bootstrap();

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./controllers/account":1,"./controllers/awards":2,"./controllers/base":3,"./controllers/catalog":4,"./controllers/dashboard":5,"./controllers/home":6,"./controllers/leaderboard":7,"./controllers/quizzes":8,"./controllers/reports":9,"./controllers/salesEntry":10}]},{},[12])


//# sourceMappingURL=main.js.map
